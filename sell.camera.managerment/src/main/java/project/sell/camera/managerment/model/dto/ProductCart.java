package project.sell.camera.managerment.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductCart {
    @With
    private String productId;
    @With
    private String productName;
    @With
    private int quantity;
    @With
    private double unitPrice;
    @With
    private String img;

}
