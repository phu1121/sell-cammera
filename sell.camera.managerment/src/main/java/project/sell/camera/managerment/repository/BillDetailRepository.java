package project.sell.camera.managerment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.sell.camera.managerment.model.entity.BillDetailEntity;

@Repository
public interface BillDetailRepository extends JpaRepository<BillDetailEntity, String> {
}
