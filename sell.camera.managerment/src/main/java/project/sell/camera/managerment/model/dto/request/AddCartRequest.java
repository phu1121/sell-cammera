package project.sell.camera.managerment.model.dto.request;

import lombok.Data;

@Data
public class AddCartRequest {
    private String productId;
    private int quantity;
}
