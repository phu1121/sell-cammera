package project.sell.camera.managerment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.sell.camera.managerment.model.entity.BillEntity;

@Repository
public interface BillRepository extends JpaRepository<BillEntity, String> {
}
