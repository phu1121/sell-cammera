package project.sell.camera.managerment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class HomeController {

    @RequestMapping(value = {"", "index"})
    public String index() {
        return "/pages/index";
    }

    @RequestMapping(value = "/about")
    public String about(Model model) {
        model.addAttribute("data", "Hello World");
        return "/pages/about";
    }

}
