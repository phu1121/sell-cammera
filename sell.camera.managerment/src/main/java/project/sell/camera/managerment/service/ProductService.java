package project.sell.camera.managerment.service;


import org.springframework.http.ResponseEntity;
import project.sell.camera.managerment.model.entity.ProductEntity;

import java.util.List;

public interface ProductService {
    List<ProductEntity> getProducts();

    ResponseEntity deleteProducts(String id);

}
