package project.sell.camera.managerment.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import project.sell.camera.managerment.model.dto.CartDto;
import project.sell.camera.managerment.model.dto.ProductCart;
import project.sell.camera.managerment.model.dto.request.OrderRequest;
import project.sell.camera.managerment.model.entity.BillDetailEntity;
import project.sell.camera.managerment.model.entity.BillEntity;
import project.sell.camera.managerment.model.entity.ClientEntity;
import project.sell.camera.managerment.model.entity.ProductEntity;
import project.sell.camera.managerment.model.mapper.PaymentMapper;
import project.sell.camera.managerment.repository.BillDetailRepository;
import project.sell.camera.managerment.repository.BillRepository;
import project.sell.camera.managerment.repository.ClientRepository;
import project.sell.camera.managerment.repository.ProductRepository;
import project.sell.camera.managerment.service.PaymentService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Resource
    private ProductRepository productRepository;
    @Resource
    private ClientRepository clientRepository;
    @Resource
    private BillRepository billRepository;
    @Resource
    private BillDetailRepository billDetailRepository;

    @Override
    public void addToCart(HttpSession httpSession, String productId, int quantity) {
        Optional<ProductEntity> optionalProductEntity = productRepository.findById(productId);
        if (!optionalProductEntity.isPresent()) {
            //Todo - Báo lỗi không tìm thấy.
            return;
        }
        ProductEntity productEntity = optionalProductEntity.get();

        CartDto cart = (CartDto) httpSession.getAttribute("CART");
        if (cart == null) {
            cart = new CartDto();
            Map<String, ProductCart> products = new HashMap<>();
            ProductCart item = new ProductCart()
                    .withProductId(productEntity.getId())
                    .withProductName(productEntity.getName())
                    .withQuantity(quantity)
                    .withUnitPrice(productEntity.getPrice())
                    .withImg(productEntity.getImg());
            products.put(productEntity.getId(), item);
            cart.setProducts(products);
        } else {
            Map<String, ProductCart> products = cart.getProducts();
            ProductCart item = products.get(productEntity.getId());
            if (item == null) {
                item = new ProductCart()
                        .withProductId(productEntity.getId())
                        .withProductName(productEntity.getName())
                        .withQuantity(quantity)
                        .withUnitPrice(productEntity.getPrice())
                        .withImg(productEntity.getImg());
                products.put(productEntity.getId(), item);
            } else {
                item.setQuantity(item.getQuantity() + quantity);
            }
        }
        httpSession.setAttribute("CART", cart);

    }

    @Override
    public CartDto getCart(HttpSession httpSession) {
        CartDto cart = (CartDto) httpSession.getAttribute("CART");
        return cart != null ? cart : new CartDto()
                .withProducts(new HashMap<>())
                .withTotalAmount(0)
                .withTotalQuantity(0);
    }

    @Override
    public boolean order(OrderRequest orderRequest) {
        PaymentMapper paymentMapper = new PaymentMapper();
        ClientEntity clientEntity = paymentMapper.mapToClientEntity(orderRequest);
        if (StringUtils.isNotBlank(orderRequest.getCustomerId())) {
            Optional<ClientEntity> optionalClientEntity = clientRepository.findById(orderRequest.getCustomerId());
            if (!optionalClientEntity.isPresent()) {
                return false;
            }
            clientEntity = optionalClientEntity.get();
        }

        List<OrderRequest.ProductRequest> products = orderRequest.getProducts();

        List<String> productIds = products.stream().map(OrderRequest.ProductRequest::getProductId).collect(Collectors.toList());
        Map<String, ProductEntity> productMap = productRepository.findAllByIdIn(productIds).stream()
                .collect(Collectors.toMap(ProductEntity::getId, productEntity -> productEntity));
        BillEntity billEntity = paymentMapper.mapToBillEntity(clientEntity.getId(), null, 0);
        List<BillDetailEntity> billDetailEntities = new ArrayList<>();
        double sum = 0;
        for (OrderRequest.ProductRequest productRequest : products) {
            ProductEntity productEntity = productMap.get(productRequest.getProductId());
            sum += productEntity.getPrice() * productRequest.getQuantity();
            BillDetailEntity billDetailEntity = paymentMapper.mapToBillDetailEntity(billEntity.getId(), productRequest.getProductId(), productRequest.getQuantity(), productEntity.getPrice());
            billDetailEntities.add(billDetailEntity);
        }
        billEntity.setTotal(sum);

        clientRepository.save(clientEntity);
        billRepository.save(billEntity);
        billDetailRepository.saveAll(billDetailEntities);
        return true;
    }

}
