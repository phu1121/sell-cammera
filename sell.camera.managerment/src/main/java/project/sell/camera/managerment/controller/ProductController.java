package project.sell.camera.managerment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import project.sell.camera.managerment.model.entity.ProductEntity;
import project.sell.camera.managerment.service.ProductService;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {
    @Resource
    private ProductService productService;

    @RequestMapping(value = "/get-list")
    public String getList(Model model) {
        List<ProductEntity> products = productService.getProducts();
        model.addAttribute("listData", products);
        return "/pages/product";
    }

    public void createdNewProduct(@RequestBody ProductEntity productEntity) {
        //Todo
    }

}
