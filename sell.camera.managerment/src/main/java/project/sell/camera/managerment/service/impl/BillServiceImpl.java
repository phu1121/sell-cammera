package project.sell.camera.managerment.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import project.sell.camera.managerment.model.entity.BillEntity;
import project.sell.camera.managerment.repository.BillRepository;
import project.sell.camera.managerment.service.BillService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class BillServiceImpl implements BillService {

    @Resource
    BillRepository billRepository;

    @Override
    public List<BillEntity> getBills() {
        return billRepository.findAll();
    }

    @Override
    public ResponseEntity deleteBills(String id) {
        try {
            Optional<BillEntity> optionalBillEntity = billRepository.findById(id);
            if (!optionalBillEntity.isPresent()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Không tìm thấy dữ liệu");

            }
            billRepository.delete(optionalBillEntity.get());
            return ResponseEntity.status(HttpStatus.OK).body("Xóa thành công");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Lối hệ thống");
        }

    }

}

