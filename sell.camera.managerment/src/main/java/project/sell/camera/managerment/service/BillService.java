package project.sell.camera.managerment.service;

import org.springframework.http.ResponseEntity;
import project.sell.camera.managerment.model.entity.BillEntity;

import java.util.List;


public interface BillService {
    List<BillEntity> getBills();

    ResponseEntity deleteBills(String id);

}
