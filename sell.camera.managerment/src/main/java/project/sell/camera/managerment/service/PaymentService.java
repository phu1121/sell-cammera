package project.sell.camera.managerment.service;

import project.sell.camera.managerment.model.dto.CartDto;
import project.sell.camera.managerment.model.dto.request.OrderRequest;

import javax.servlet.http.HttpSession;

public interface PaymentService {
    void addToCart(HttpSession httpSession, String productId, int quantity);

    CartDto getCart(HttpSession httpSession);

    boolean order(OrderRequest orderRequest);
}
