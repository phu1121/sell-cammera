package project.sell.camera.managerment.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import project.sell.camera.managerment.model.entity.StaffEntity;
import project.sell.camera.managerment.repository.StaffRepository;
import project.sell.camera.managerment.service.StaffService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class StaffServiceImpl implements StaffService {
    @Resource
    StaffRepository staffRepository;

    @Override
    public List<StaffEntity> getStaffs() {
        return staffRepository.findAll();
    }

    @Override
    public ResponseEntity deleteStaffs(String id) {
        try {
            Optional<StaffEntity> optionalStaffEntity = staffRepository.findById(id);
            if (!optionalStaffEntity.isPresent()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Không tìm thấy dữ liệu");
            }
            staffRepository.delete(optionalStaffEntity.get());
            return ResponseEntity.status(HttpStatus.OK).body("Xóa thành công");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Lỗi hệ thống");
        }
    }


}

