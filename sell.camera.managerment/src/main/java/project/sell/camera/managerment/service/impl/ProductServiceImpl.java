package project.sell.camera.managerment.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import project.sell.camera.managerment.model.entity.ProductEntity;
import project.sell.camera.managerment.repository.ProductRepository;
import project.sell.camera.managerment.service.ProductService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Resource
    ProductRepository productRepository;

    @Override
    public List<ProductEntity> getProducts() {
        return productRepository.findAll();
    }

    @Override
    public ResponseEntity deleteProducts(String id) {
        try {
            Optional<ProductEntity> optionalProductEntity = productRepository.findById(id);
            if (!optionalProductEntity.isPresent()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Không tìm thấy dữ liệu");
            }
            productRepository.delete(optionalProductEntity.get());
            return ResponseEntity.status(HttpStatus.OK).body("Xóa thành công");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Lỗi hệ thống");
        }

    }
}
