package project.sell.camera.managerment.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import project.sell.camera.managerment.model.dto.CartDto;
import project.sell.camera.managerment.model.dto.request.AddCartRequest;
import project.sell.camera.managerment.model.dto.request.OrderRequest;
import project.sell.camera.managerment.service.PaymentService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/payment")
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @RequestMapping(
            value = "/add-to-cart",
            method = RequestMethod.POST
    )
    @ResponseStatus(HttpStatus.OK)
    public void addToCart(@RequestBody AddCartRequest request, HttpSession httpSession) {
        paymentService.addToCart(httpSession, request.getProductId(), request.getQuantity());
    }

    @RequestMapping(
            value = "/get-cart",
            method = RequestMethod.GET
    )
    public String getCart(HttpSession httpSession, Model model) {
        CartDto cart = paymentService.getCart(httpSession);
        model.addAttribute("cart", cart);
        return "/fragment/quick-cart";
    }

    @RequestMapping(
            value = "/check-out",
            method = RequestMethod.GET
    )
    public String checkOut(HttpSession httpSession, Model model) {
        CartDto cart = paymentService.getCart(httpSession);
        model.addAttribute("cart", cart);
        return "pages/check-out";
    }

    @RequestMapping(
            value = "/order",
            method = RequestMethod.POST
    )
    public String order(@RequestBody OrderRequest orderRequest, HttpSession httpSession) {
        boolean isOrderSuccess = paymentService.order(orderRequest);
        if (isOrderSuccess) {
            httpSession.setAttribute("CART", null);
        }
        return "redirect:/index";
    }
}
