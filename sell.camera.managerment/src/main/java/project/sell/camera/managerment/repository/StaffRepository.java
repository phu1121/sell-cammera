package project.sell.camera.managerment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.sell.camera.managerment.model.entity.StaffEntity;

@Repository
public interface StaffRepository extends JpaRepository<StaffEntity ,String>{

}
