package project.sell.camera.managerment.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest {
    private List<ProductRequest> products;
    private String customerId;
    private String customerName;
    private String phoneNumber;
    private String address;
    private String note;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProductRequest {
        private String productId;
        private int quantity;
    }
}
