package project.sell.camera.managerment.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartDto {
    @With
    private Map<String, ProductCart> products;
    @With
    private int totalQuantity;
    @With
    private float totalAmount;
}
