package project.sell.camera.managerment.service;

import org.springframework.http.ResponseEntity;
import project.sell.camera.managerment.model.entity.StaffEntity;

import java.util.List;

public interface StaffService {
    List<StaffEntity> getStaffs();

    ResponseEntity deleteStaffs(String id);

}
