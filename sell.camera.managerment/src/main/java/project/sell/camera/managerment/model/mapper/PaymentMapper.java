package project.sell.camera.managerment.model.mapper;

import project.sell.camera.managerment.model.dto.request.OrderRequest;
import project.sell.camera.managerment.model.entity.BillDetailEntity;
import project.sell.camera.managerment.model.entity.BillEntity;
import project.sell.camera.managerment.model.entity.ClientEntity;
import project.sell.camera.managerment.type.BillStatus;
import project.sell.camera.managerment.type.CustomerStatus;

import java.sql.Timestamp;
import java.util.UUID;

public class PaymentMapper {
    public ClientEntity mapToClientEntity(OrderRequest orderRequest) {
        ClientEntity entity = new ClientEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setName(orderRequest.getCustomerName());
        entity.setAddress(orderRequest.getAddress());
        entity.setPhone(orderRequest.getPhoneNumber());
        entity.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        entity.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
        entity.setStatus(CustomerStatus.UNREGISTER.name());
        return entity;
    }

    public BillEntity mapToBillEntity(String clientId, String staffId, double sum) {
        BillEntity entity = new BillEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setClientId(clientId);
        entity.setStaffId(staffId);
        entity.setTotal(sum);
        entity.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        entity.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
        entity.setStatus(BillStatus.ACTIVE.name());
        return entity;
    }

    public BillDetailEntity mapToBillDetailEntity(String billId, String productId, int quantity, double unitPrice) {
        BillDetailEntity entity = new BillDetailEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setBillId(billId);
        entity.setProductId(productId);
        entity.setQuantity(quantity);
        entity.setUnitPrice(unitPrice);
        return entity;
    }
}
