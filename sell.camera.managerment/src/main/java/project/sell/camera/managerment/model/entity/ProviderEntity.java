package project.sell.camera.managerment.model.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "PROVIDER", schema = "dbo", catalog = "QLBH")
public class ProviderEntity {
    private String id;
    private String providerName;
    private String address;
    private String producerName;
    private String status;

    @Id
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "PROVIDER_NAME")
    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    @Basic
    @Column(name = "ADDRESS")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "PRODUCER_NAME")
    public String getProducerName() {
        return producerName;
    }

    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    @Basic
    @Column(name = "STATUS")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProviderEntity that = (ProviderEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(providerName, that.providerName) &&
                Objects.equals(address, that.address) &&
                Objects.equals(producerName, that.producerName) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, providerName, address, producerName, status);
    }
}
